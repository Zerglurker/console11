﻿// ConsoleApplicationTest.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Helpers.h"

//вызывает std:cout
void print(float a){

	std::cout << a << "\n";

}


/*  Урок 13.4
	@Author:Zerglurker000@gmail.com
*/
int main()
{
	print(FuncSqr(13.4, 23.23));

	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"